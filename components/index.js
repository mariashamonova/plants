import Vue from 'vue';

import vuetify from '@/plugins/vuetify'
import AppLogo from "./components/AppLogo";
import Catalog from "./components/shop/Catalog";
import Footer from './components/Footer.vue';
import LoginForm from './components/registration/LoginForm';
import Register from './registration/Register';
import AllProduct from './adminPanel/AllProduct';
import Product from './adminPanel/Product';
import AddProduct from './adminPanel/AddProduct';
import AddCategory from './adminPanel/AddCategory';
import AddSize from './adminPanel/AddSize';
import AddColor from './adminPanel/AddColor';
import BasketTotal from './basket/BasketTotal';
import EditPlant from './adminPanel/EditPlant';
import AddPosColor from './adminPanel/edit/AddPosColor';
import AddPosSize from './adminPanel/edit/AddPosSize';
import PlantInfo from './shop/PlantInfo';
import BasketProduct from './basket/BasketProduct'
import DeliveryForm from './basket/DeliveryForm'
import ActiveOrders from './basket/ActiveOrders'

Vue.component('app-logo', AppLogo);
Vue.component('app-catalog', Catalog);
Vue.component('app-footer', Footer);
Vue.component('login-form', LoginForm);
Vue.component('register-form', Register);
Vue.component('all-product', AllProduct);
Vue.component('product', Product);
Vue.component('add-product', AddProduct);
Vue.component('add-category', AddCategory);
Vue.component('add-color', AddColor);
Vue.component('basket-total', BasketTotal);
Vue.component('edit-plant', EditPlant);
Vue.component('add-pos-col', AddPosColor);
Vue.component('add-pos-size', AddPosSize);
Vue.component('plant-info', PlantInfo);
Vue.component('basket-item', BasketProduct);
Vue.component('delivery-form', DeliveryForm);
Vue.component('active-orders', ActiveOrders);