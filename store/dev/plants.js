import _ from 'lodash'
import { Api } from '~/plugins/axios-rest-client';
import axios from 'axios';

const SET_ITEMS = 'SET_ITEMS'
const CLEAR_ITEMS = 'CLEAR_ITEMS'
const CREATE_ITEM = 'CREATE_ITEM'
const EDIT_ITEM = 'EDIT_ITEM'
const REMOVE_ITEM = 'REMOVE_ITEM'


export const state = () => ({
  items: [],
  count: null,
  
})

export const getters = {
  // items: state => (statuses) => {
  //   return _.map(state.items, (el, i) => {
  //     el.color = _.find(statuses, (st) => st.id === el.status).color;
  //     return el
  //   });
  // },
  items: state => state.items,
  byId: state => (id) => _.find(state.items, (el) => el.id === id),
  count: state => { 
    return state.count; 
  }
 
}

export const actions = {

  create ({ commit }, payload) {
    commit('CREATE_ITEM', payload)
  },
  edit ({ commit }, payload) {
    commit('EDIT_ITEM', payload)
  },
  remove ({ commit }, id) {
    commit('REMOVE_ITEM', id)
  },
  async getPlants({ commit }, filters) {
    let res;
  
    let title = filters.title ? "/"+filters.title:'';
    console.log(filters)
    let price; 
    price = filters.sortPrice ?  filters.sortPrice : null;
    return axios('http://127.0.0.1:8000/api/plants'+ title , {
      method: "GET",
      params: {
        size: filters.sizes,
        color: filters.colors,
        category: filters.size,
        price: price,
        min: filters.minPrice,
        max: filters.maxPrice,
        paggination: {
          limit: filters.limit !== undefined ? filters.limit : null,
          offset: filters.offset!== undefined ? String(filters.offset) : null
        },
        all_plants: filters.all_plants
        
      }
    }).then((response) => {
      //console.log(response.data)
      commit('SET_ITEMS', response.data);
    }).catch(function (error) {
             console.log(error, error.response);
          })

  },
}

export const mutations = {
  [SET_ITEMS] (state, payload) {
    console.log(payload)
    
    
    state.count = payload[payload.length - 1].count;
    payload.splice(-1,1)
    
    state.items = payload
  },
  [CLEAR_ITEMS] (state) {
    state.items = []
  },
  [CREATE_ITEM] (state, payload) {

  
    axios('http://127.0.0.1:8000/api/plants',{
      method: "POST",
      params: payload
    })
      state.items.push(payload)
    
  },
  [EDIT_ITEM] (state, payload) {
    console.log(payload)
    let index = _.findIndex(state.items, el => el.id === payload.id)

    axios('http://127.0.0.1:8000/api/plants'+ '/'+  payload.id ,{
      method: "PUT",
      params: payload
    }).then(res => {
      state.items[index] = payload;
    }) 
  },
  [REMOVE_ITEM] (state, articul) {
    
    let index = _.findIndex(state.items, el => el.articul === articul);
    axios('http://127.0.0.1:8000/api/plants'+ '/'+ state.items[index].id, {
      method: "DELETE",
    })
    state.items.splice(index, 1)
  }
}
