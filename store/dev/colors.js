import _ from 'lodash';
import { Api } from '~/plugins/axios-rest-client';
import axios from 'axios';

const SET_ITEMS = 'SET_ITEMS';
const CREATE_ITEM = 'CREATE_ITEM';
const REMOVE_ITEM = 'REMOVE_ITEM';
const EDIT_ITEM = 'EDIT_ITEM';

export const state = () => ({
  items: [],
});


export const getters = {
  items: state => state.items,
  byId: state => (id) => _.find(state.items, (el) => el.id === id)
}

export const actions = {
  async getColor({ commit }, filters) {

    let res;
   
    return axios('http://127.0.0.1:8000/api/colors', {
      method: "GET"
    }).then((res) => {
      console.log(res.data)
      commit('SET_ITEMS', res.data)
    })

  },
  create ({ commit }, payload) {
    commit('CREATE_ITEM', payload)
  },
  edit ({ commit }, payload) {
    commit('EDIT_ITEM', payload)
  },
  remove ({ commit }, id) {
    commit('REMOVE_ITEM', id)
  }
}

export const mutations = {
  [SET_ITEMS] (state, payload) {
    state.items = payload;
  },
  [CREATE_ITEM] (state, payload) {
    axios('http://127.0.0.1:8000/api/colors',{
      method: "POST",
      params: payload
    })
    state.items.push({
      id: state.items.length ? state.items[state.items.length - 1].id + 1 : 1,
      color: payload.color,
      created_at: new Date,
      update_at: new Date()
    })
  },
  [EDIT_ITEM] (state, payload) {
    let index = _.findIndex(state.items, el => el.id === payload.id);
    state.items[index] = payload;
  },
  [REMOVE_ITEM] (state, id) {
    let index = _.findIndex(state.items, el => el.id === id)
    state.items.splice(index, 1)
  }
 
};
